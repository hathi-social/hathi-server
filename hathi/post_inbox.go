package hathi

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"gitlab.com/hathi-social/hathi-server/activitypub"
	//"fmt"
	"database/sql"
	"log"
	"net/http"
)

func (c *Core) HandleInboxActivity(tx *sql.Tx, remoteP bool, inboxID string, actorID string, activity *activitypub.Object) (err error) {
	if tx == nil {
		tx, err = c.db.BeginTransaction()
		if err != nil {
			return err
		}
		defer c.db.EndTransaction(tx, &err)
	}
	if remoteP == false {
		// have it locally, only need to make sure this Actor is authorized
	} else {
		// Need to shadow the object
		activity.SetField("shadow", activity.GetID())
		activity.SetField("@id", c.NewID())
	}
	// send to handlers
	activityType, _ := activity.GetFieldAsString("@type")
	switch activityType {
	case "Accept":
		c.HandleInAccept(tx, remoteP, activity)
	case "Add":
	case "Announce":
	case "Arrive":
	case "Block":
	case "Create":
		c.HandleInCreate(tx, remoteP, activity)
	case "Delete":
	case "Dislike":
	case "Flag":
	case "Follow":
		c.HandleInFollow(tx, remoteP, activity)
	case "Ignore":
	case "Invite":
	case "Join":
	case "Leave":
	case "Like":
	case "Listen":
	case "Move":
	case "Offer":
	case "Question":
	case "Reject":
		c.HandleInReject(tx, remoteP, activity)
	case "Read":
	case "Remove":
	case "TentativeReject":
	case "TentativeAccept":
		c.HandleInTentativeAccept(tx, remoteP, activity)
	case "Travel":
	case "Undo":
	case "Update":
	case "View":
	}
	activityID := activity.GetID()
	if remoteP == true {
		// We wait to store a shadow as it may be changed by the handler
		c.db.StoreObject(tx, activity)
		// Distribute replies. Local propogations already handled this.
		replyList := activity.GetField("inReplyTo").([]interface{})
		if replyList != nil {
			for _, replyTarget := range replyList {
				isRemote, err := c.IsIDRemote(replyTarget.(string))
				if err != nil {
					continue // Skip this?
				}
				if isRemote == false {
					c.HandleReply(tx, activityID, replyTarget.(string))
				}
			}
		}
	}
	c.db.ListAppend(tx, inboxID, "items", activityID)
	return nil
}

func (c *Core) PostInbox(remoteP bool, actorID string, activity *activitypub.Object) (statusCode int, externalError string, err error) {
	tx, err := c.db.BeginTransaction()
	defer c.db.EndTransaction(tx, &err)
	actor, err := c.db.LoadObject(tx, actorID)
	if err != nil {
		return http.StatusInternalServerError, "", err
	}
	if actor == nil {
		// No Actor, or any object at all here
		if Logging {
			log.Println("Inbox POST error; no actor, or anything")
		}
		return http.StatusNotFound, "", nil
	}
	if actor.IsActor() == false {
		// Not an Actor
		if Logging {
			log.Println("Inbox POST error; no actor")
		}
		return http.StatusBadRequest, "", nil
	}
	inboxID := actor.GetField("inbox").(string)
	err = c.HandleInboxActivity(tx, remoteP, inboxID, actorID, activity)
	if err == nil {
		return http.StatusCreated, "", nil
	} else {
		return http.StatusBadRequest, "", nil
	}
}
