package hathi

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"gitlab.com/hathi-social/hathi-server/activitypub"
	//"fmt"
)

// Ensures that two lists contain the same items, no enforced order
func syncLists(source *[]string, dest *[]string, bothDir bool) {
	// Make sure we have lists to sync
	if *source == nil && *dest == nil {
		return // No lists, nothing to do here
	} else if *source == nil && *dest != nil {
		*source = []string{}
	} else if *source != nil && *dest == nil {
		*dest = []string{}
	}
	// Sync up
	for _, srcVal := range *source {
		found := false
		for _, destVal := range *dest {
			if srcVal == destVal {
				found = true
			}
		}
		if found == false {
			*dest = append(*dest, srcVal)
		}
	}
	if bothDir == true {
		syncLists(dest, source, false)
	}
}

func preparePropList(lst []string, originID string) (newL []string) {
	tmp := make(map[string]bool)
	newL = []string{}
	for _, value := range lst {
		if value == originID { // Don't want to send someone their own message
			// TODO: may remove this, unsure.
			continue
		}
		_, ok := tmp[value]
		if !ok { // Haven't seen this value yet
			tmp[value] = true
			newL = append(newL, value)
		}
	}
	return newL
}

func makeCreateWrapper(actorID string, object *activitypub.Object) (cre *activitypub.Object) {
	cre = activitypub.NewObject()
	cre.SetField("@type", "Create")
	// Propagation and distribution information should be the same for both
	// the Create and the created Object
	temp := object.GetField("to")
	if temp != nil {
		cre.SetField("to", temp)
	}
	temp = object.GetField("bto")
	if temp != nil {
		cre.SetField("bto", temp)
	}
	temp = object.GetField("cc")
	if temp != nil {
		cre.SetField("cc", temp)
	}
	temp = object.GetField("bcc")
	if temp != nil {
		cre.SetField("bcc", temp)
	}
	temp = object.GetField("audience")
	if temp != nil {
		cre.SetField("audience", temp)
	}
	temp = object.GetField("published")
	if temp != nil {
		cre.SetField("published", temp)
	}
	cre.SetField("origin", actorID)
	cre.SetField("object", object)
	return cre
}
