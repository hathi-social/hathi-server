package activitypub

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
//"fmt"
)

var ActivityTypes = map[string]bool{
	"Activity":        true,
	"Accept":          true,
	"Add":             true,
	"Announce":        true,
	"Arrive":          true,
	"Block":           true,
	"Create":          true,
	"Delete":          true,
	"Dislike":         true,
	"Flag":            true,
	"Follow":          true,
	"Ignore":          true,
	"Invite":          true,
	"Join":            true,
	"Leave":           true,
	"Like":            true,
	"Listen":          true,
	"Move":            true,
	"Offer":           true,
	"Question":        true,
	"Reject":          true,
	"Read":            true,
	"Remove":          true,
	"TentativeReject": true,
	"TentativeAccept": true,
	"Travel":          true,
	"Undo":            true,
	"Update":          true,
	"View":            true,
}

var ActorTypes = map[string]bool{
	"Actor":        true,
	"Application":  true,
	"Group":        true,
	"Organization": true,
	"Person":       true,
	"Service":      true,
}

var CollectionTypes = map[string]bool{
	"Collection":        true,
	"OrderedCollection": true,
	"Availability":      true,
}

// strings are single-value, []strings are multi-value
// map[string]strings are language maps
type Object struct {
	Id string // Set to @id if it exists, embed ID otherwise
	// Object data
	Data map[string]interface{}
	// Other data, *not* JSONified
	Shadow string // The remote object this is a cache of
}

func NewObject() (obj *Object) {
	obj = &Object{}
	obj.Data = map[string]interface{}{}
	return obj
}

func (o *Object) SetID(objID string) {
	o.Id = objID
	o.SetField("@id", objID)
}

func (o *Object) GetID() (objID string) {
	if o.Id == "" {
		objID, _ = o.GetFieldAsString("@id")
		if objID == "" {
			objID, _ = o.GetFieldAsString("id")
		}
	} else {
		objID = o.Id
	}
	return objID
}

func (o *Object) IsActor() bool {
	typeData := o.GetField("@type")
	if typeData == "" {
		typeData = o.GetField("type")
	}
	switch typedV := typeData.(type) {
	case []interface{}:
		// TODO: loop through items
	case string:
		_, ok := ActorTypes[typedV]
		return ok
	}
	return false
}

func (o *Object) IsActivity() bool {
	typeData := o.GetField("@type")
	if typeData == "" {
		typeData = o.GetField("type")
	}
	switch typedV := typeData.(type) {
	case []interface{}:
		// TODO: loop through items
	case string:
		_, ok := ActivityTypes[typedV]
		return ok
	}
	return false
}

func (o *Object) IsCollection() bool {
	typeData := o.GetField("@type")
	if typeData == "" {
		typeData = o.GetField("type")
	}
	switch typedV := typeData.(type) {
	case []interface{}:
		// TODO: loop through items
	case string:
		_, ok := CollectionTypes[typedV]
		return ok
	}
	return false
}

// TODO: HasType(typeName) loops through type list to find type
// TODO: What shortcut functions do we want?

func (o *Object) Mapify() (mapped map[string]interface{}) {
	// Turn an Object (possibly with sub-Objects) into a map for JSONification
	mapped = make(map[string]interface{})
	// Loop through Data, add to map.
	for fieldName, v := range o.Data {
		mapped[fieldName] = mapifyItem(v)
	}
	// If an item is an Object then call its Mapify() method
	return mapped
}

func mapifyItem(value interface{}) (result interface{}) {
	// Properly handle sub-objects and lists
	switch typedV := value.(type) {
	case *Object:
		return typedV.Mapify()
	case Object:
		return typedV.Mapify()
	case []interface{}:
		list := make([]interface{}, len(typedV))
		for index, item := range typedV {
			list[index] = mapifyItem(item)
		}
		return list
	default:
		return value
	}
}

func Objectify(data map[string]interface{}) (obj *Object, report *ValidationReport) {
	obj = new(Object)
	obj.Data = make(map[string]interface{}, 0)
	report = new(ValidationReport)
	report.NoType = true
	report.TypeAlias = false
	report.InvalidFieldNames = make([]string, 0)
	report.UnknownFieldNames = make([]string, 0)
	report.BadFieldTypes = make([]string, 0)
	// Check special fields, copying still happens in the conversion loop
	// Extract @context
	var aliases map[string]string
	if context, ok := data["@context"]; ok {
		aliases = ValidateContext(context, report)
	} else {
		aliases = make(map[string]string, 0)
		report.MissingContext = true
	}
	// Check existence of @id
	_, report.IdAlias = data["id"]
	_, atID := data["@id"]
	report.NoId = (report.IdAlias == false) && (atID == false)
	// Check existence of @type
	_, report.TypeAlias = data["type"]
	_, atType := data["@type"]
	report.NoType = (report.TypeAlias == false) && (atType == false)
	// Validation loop
	map2objConversionLoop(obj.Data, data, aliases, report)
	return obj, report
}

func map2objConversionLoop(objData map[string]interface{}, data map[string]interface{}, aliases map[string]string, report *ValidationReport) {
	for field, value := range data {
		if field == "@context" {
			// Context does not follow the normal rules
			objData[field] = value
			continue
		} else if field == "@type" || field == "type" {
			validType := ValidateType(value, aliases)
			if validType == false {
				report.ContainsInvalidType = true
			}
			objData[field] = value
			continue
		}
		// Validate the field
		validName, impField, validType := ValidateField(field, value, aliases)
		if validName == false {
			report.InvalidFieldNames = append(report.InvalidFieldNames, field)
		}
		if impField == false {
			report.UnknownFieldNames = append(report.UnknownFieldNames, field)
		}
		if (impField == true) && (validType == false) {
			report.BadFieldTypes = append(report.BadFieldTypes, field)
		}
		// if value is a sub-object we need to recurse
		switch typedV := value.(type) {
		case map[string]interface{}:
			_, hasType := typedV["type"]
			_, hasAtType := typedV["@type"]
			if hasType || hasAtType {
				// Objectify and store
				subObj, _ := Objectify(typedV)
				objData[field] = subObj
			} else {
				// Not an object; recurse and store
				subObj := make(map[string]interface{}, 0)
				map2objConversionLoop(subObj, typedV, aliases, report)
				objData[field] = subObj
			}
		default:
			objData[field] = value
		}
	}
}

func (o *Object) GetField(fieldName string) (value interface{}) {
	if value, ok := o.Data[fieldName]; ok {
		return value
	} else {
		return nil
	}
}

func (o *Object) GetFieldAsString(fieldName string) (value string, otherValue bool) {
	temp := o.GetField(fieldName)
	if temp == nil { // No value present
		return "", false
	}
	switch typedV := temp.(type) {
	case string:
		return typedV, false
	default: // Something is present but it is not a string
		return "", true
	}
}

func (o *Object) GetFieldAsStringList(fieldName string) (value []string, otherValue bool) {
	temp := o.GetField(fieldName)
	if temp == nil { // No value present
		return nil, false
	}
	value = make([]string, 0)
	switch typedV := temp.(type) {
	case []string:
		return typedV, false
	case []interface{}:
		for _, v := range typedV {
			switch typedItem := v.(type) {
			case string:
				value = append(value, typedItem)
			default:
				// Silently drop non-strings, or fail?
			}
		}
		return value, false
	case string:
		value = append(value, typedV)
		return value, false
	default:
		return nil, true
	}
}

func (o *Object) SetField(fieldName string, value interface{}) {
	o.Data[fieldName] = value
}
