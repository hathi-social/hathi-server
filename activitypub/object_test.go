package activitypub

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	//"fmt"
	"reflect"
	"testing"
)

func TestObjectify(t *testing.T) {
	// Test perfect
	sourceData := map[string]interface{}{
		"@context": "https://www.w3.org/ns/activitystreams",
		"@id":      "https://foo.test/1", "@type": "Create",
		"object": map[string]interface{}{"@type": "Image",
			"icon": "http://bar.test/bar.png"}}
	targetData := map[string]interface{}{
		"@context": "https://www.w3.org/ns/activitystreams",
		"@id":      "https://foo.test/1", "@type": "Create",
		"object": &Object{
			Data: map[string]interface{}{"@type": "Image",
				"icon": "http://bar.test/bar.png"}}}
	obj, report := Objectify(sourceData)
	// first level data
	if obj.Data["@context"] != targetData["@context"] ||
		obj.Data["@id"] != targetData["@id"] ||
		obj.Data["@type"] != targetData["@type"] {
		t.Fatal("ConvertMapToObject object failure:", obj)
	}
	// embedded object data
	subData := obj.Data["object"].(*Object).Data
	targetSubData := targetData["object"].(*Object).Data
	if subData["@type"] != targetSubData["@type"] ||
		subData["icon"] != targetSubData["icon"] {
		t.Fatal("ConvertMapToObject sub-object failure:", obj)
	}
	if report == nil {
		t.Fatal("ConvertMapToObject no report")
	}
	if report.MissingContext != false || report.InvalidContext != false ||
		report.MissingVocab != false || report.ImproperVocab != false ||
		report.DefaultLanguage != "" || report.NoType != false ||
		report.TypeAlias != false || report.NoId != false ||
		report.IdAlias != false ||
		reflect.DeepEqual(report.InvalidFieldNames, &[]string{}) ||
		reflect.DeepEqual(report.UnknownFieldNames, &[]string{}) ||
		reflect.DeepEqual(report.BadFieldTypes, &[]string{}) {
		t.Fatal("ConvertMapToObject report failure:", *report)
	}
}

func TestMapify(t *testing.T) {
	obj := Object{Data: map[string]interface{}{
		"id": "hathi.rapture.test", "int": 42, "float": 1.25,
		"bool": true, "many": []interface{}{"bar", 23},
		"sub": map[string]interface{}{"baz": "quux"}},
	}
	mapped := obj.Mapify()
	if (len(mapped) != 6) || (mapped["id"].(string) != "hathi.rapture.test") ||
		(mapped["int"].(int) != 42) || (mapped["float"].(float64) != 1.25) ||
		(mapped["bool"].(bool) != true) ||
		(mapped["many"].([]interface{})[0].(string) != "bar") ||
		(mapped["many"].([]interface{})[1].(int) != 23) ||
		(mapped["sub"].(map[string]interface{})["baz"].(string) != "quux") {
		t.Fatal("TestMapify equality error:", mapped)
	}
}
