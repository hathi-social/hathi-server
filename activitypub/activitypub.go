package activitypub

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"encoding/json"
	//"fmt"
)

var Logging bool

func DecodeJSON(data []byte) (obj *Object, err error) {
	// Tokenizes the raw JSON string into a dict which can be easily parsed,
	// then sends that dict to the actual decoder.
	var tmp interface{}
	err = json.Unmarshal(data, &tmp)
	if err != nil {
		return nil, err
	}
	jsonData := tmp.(map[string]interface{})
	// TODO: handle validation report
	obj, _ = Objectify(jsonData)
	return obj, nil
}
