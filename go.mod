module gitlab.com/hathi-social/hathi-server

require (
	github.com/mattn/go-sqlite3 v1.10.0
	github.com/peterhellberg/duration v0.0.0-20190124114432-484232d632c1
	gitlab.com/hathi-social/hathi-protocol v0.0.0-20190426171836-6113556a314b
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.0
)
