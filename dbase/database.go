package dbase

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	//"fmt"
	//"log"
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	//"gitlab.com/hathi-social/hathi-server/activitypub"
)

var Logging bool

// The Go DB is wrapped so that other code is as isolated as possible from
// the details of what database type is being dealt with. There may be other
// data to track in future so we save on transition costs if that happens.
// The cost is that we have to implement methods like Close()
type HathiDB struct {
	DB *sql.DB
}

func OpenDatabase(dbtype string, filename string) (db *HathiDB, err error) {
	db = new(HathiDB)
	db.DB, err = sql.Open(dbtype, filename)
	if err != nil {
		return nil, err
	}
	return db, err
}

func (h *HathiDB) Initialize() (err error) {
	tx, err := h.DB.Begin()
	if err != nil {
		return err
	}

	defer func() {
		switch err {
		case nil:
			err = tx.Commit()
		default:
			tx.Rollback()
		}
	}()
	// Create tables
	_, err = h.DB.Exec(`create table FIELDS (ID text, NAME text, SINGLE bit);`)
	if err != nil {
		return err
	}
	_, err = h.DB.Exec(`create table FIELDDATA (ID text, NAME text, ITEMINDEX int, TYPE int, STRING text, INTEGER bigint, FLOAT decimal, BOOL bit, EMBED text);`)
	if err != nil {
		return err
	}
	// TRANSID used for transient ID generators
	_, err = h.DB.Exec(`create table TRANSID (LASTID bigint);`)
	if err != nil {
		return err
	}
	_, err = h.DB.Exec(`create table PRIVILEGE (ID text, ACTOR text, BITNAME string);`)
	if err != nil {
		return err
	}

	return nil
}

func (h *HathiDB) Close() (err error) {
	err = h.DB.Close()
	if err != nil {
		return err
	}
	h.DB = nil
	return nil
}

func (h *HathiDB) BeginTransaction() (tx *sql.Tx, err error) {
	tx, err = h.DB.Begin()
	if err != nil {
		return nil, err
	}
	return tx, nil
}

func (h *HathiDB) EndTransaction(tx *sql.Tx, errIn *error) (err error) {
	if errIn == nil {
		err = tx.Commit()
	} else {
		switch *errIn {
		case nil:
			err = tx.Commit()
		default:
			err = tx.Rollback()
		}
	}
	return err
}

func (h *HathiDB) GetAllObjectIDs(tx *sql.Tx) (ids []string, err error) {
	return h.getAllTableIDs(tx, "OBJECTS")
}

func (h *HathiDB) GetAllPrivilegeIDs(tx *sql.Tx) (ids []string, err error) {
	return h.getAllTableIDs(tx, "PRIVILEGE")
}
