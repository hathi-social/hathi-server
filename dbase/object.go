package dbase

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"database/sql"
	"errors"
	"reflect"
	"sort"

	"gitlab.com/hathi-social/hathi-server/activitypub"
	//"fmt"
)

// =======
// Writers
// =======

func (h *HathiDB) StoreObject(tx *sql.Tx, obj *activitypub.Object) (err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return err
		}
		defer h.EndTransaction(tx, &err)
	}
	var id string
	id = obj.GetID()
	err = h.StoreObjectData(tx, id, obj.Data)
	if err != nil {
		return err
	}
	return nil
}

func (h *HathiDB) StoreObjectData(tx *sql.Tx, objID string, objData map[string]interface{}) (err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return err
		}
		defer h.EndTransaction(tx, &err)
	}
	// Clear old data
	err = h.DeleteObjectData(tx, objID)
	if err != nil {
		return err
	}
	// Sort the fields for testability
	sortedFields := make([]string, len(objData))
	i := 0
	for fieldName, _ := range objData {
		sortedFields[i] = fieldName
		i++
	}
	sort.Strings(sortedFields)
	// Store fields
	for _, field := range sortedFields {
		err = h.StoreField(tx, objID, field, objData[field])
		if err != nil {
			return err
		}
	}
	return nil
}

func (h *HathiDB) StoreField(tx *sql.Tx, objID string, name string, value interface{}) (err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return err
		}
		defer h.EndTransaction(tx, &err)
	}
	// Is the field a scalar or a list?
	single := 1
	t := reflect.ValueOf(value)
	switch t.Kind() {
	case reflect.Slice:
		fallthrough
	case reflect.Array:
		single = 0
	}
	// Store FIELDS data
	_, err = tx.Exec(
		"insert into FIELDS (ID, NAME, SINGLE) values ($1, $2, $3)",
		objID, name, single,
	)
	if err != nil {
		return err
	}
	// Store FIELDDATA
	if single == 1 {
		h.StoreFieldItem(tx, objID, name, 0, value)
		if err != nil {
			return err
		}
	} else {
		list, err := ListToInterfaceList(value)
		if err != nil {
			return err
		}
		for index, item := range list {
			h.StoreFieldItem(tx, objID, name, index, item)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (h *HathiDB) StoreFieldItem(tx *sql.Tx, objID string, name string, index int, item interface{}) (err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return err
		}
		defer h.EndTransaction(tx, &err)
	}
	// De-multiplex
	tC, vS, vI, vF, vB, vE, err := ValueToTypes(item)
	if err != nil {
		return err
	}
	// Check for embed, do we need to recurse?
	if tC == EMBED {
		// does it have an ID, if not generate it
		// generating an ID can happen with: special, transient, complex
		// special is handled up-stack from here
		// transient and complex can use embed IDs
		if vE == "" {
			vE, err = h.GenerateIDCode(tx, "embed")
			if err != nil {
				return err
			}
			vE = "^" + vE
		}
		// Store the sub-Object
		switch typedV := item.(type) {
		case map[string]interface{}:
			err = h.StoreObjectData(tx, vE, typedV)
		case *activitypub.Object:
			typedV.Id = vE
			err = h.StoreObject(tx, typedV)
		}
		if err != nil {
			return err
		}
	}
	// Store
	_, err = tx.Exec("insert into FIELDDATA (ID, NAME, ITEMINDEX, TYPE, STRING, INTEGER, FLOAT, BOOL, EMBED) values ($1, $2, $3, $4, $5, $6, $7, $8, $9)",
		objID, name, index, tC, vS, vI, vF, vB, vE)
	if err != nil {
		return err
	}
	return nil
}

func (h *HathiDB) DeleteObjectData(tx *sql.Tx, objID string) (err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return err
		}
		defer h.EndTransaction(tx, &err)
	}
	// Delete FIELDS
	_, err = tx.Exec("delete from FIELDS where ID = $1;", objID)
	if err != nil {
		return err
	}
	// Get FIELDDATA, we don't care about the field names.
	query := "select TYPE, STRING, INTEGER, FLOAT, BOOL, EMBED from FIELDDATA where ID = $1"
	data, err := tx.Query(query, objID)
	if err != nil {
		return err
	}
	// for items that are EMBEDs, recurse and delete transients
	defer data.Close()
	var vType int
	var value interface{}
	for data.Next() {
		value, vType, err = loadValue(data)
		if err != nil {
			return err
		}
		if vType == EMBED {
			if value.(string)[0] == '^' { // transient object ID
				// Delete sub-object
				err = h.DeleteObjectData(tx, value.(string))
				if err != nil {
					return err
				}
			}
		}
	}
	// Delete FIELDDATA
	// all embeds are gone so we can nuke it all at once
	_, err = tx.Exec("delete from FIELDDATA where ID = $1;", objID)
	if err != nil {
		return err
	}
	return nil
}

// =======
// Readers
// =======

func (h *HathiDB) LoadObject(tx *sql.Tx, objID string) (obj *activitypub.Object, err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return nil, err
		}
		defer h.EndTransaction(tx, &err)
	}
	data, err := h.LoadObjectData(tx, objID)
	if err != nil {
		return nil, err
	}
	if (data == nil) || (len(data) == 0) {
		return nil, nil // No object of that ID
	}
	obj, _ = activitypub.Objectify(data)
	return obj, nil
}

func (h *HathiDB) LoadObjectData(tx *sql.Tx, objID string) (objData map[string]interface{}, err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return nil, err
		}
		defer h.EndTransaction(tx, &err)
	}
	// Get FIELDS data
	fields, err := tx.Query("select NAME, SINGLE from FIELDS where ID = $1",
		objID)
	if err != nil {
		return nil, err
	}
	defer fields.Close()
	type fieldNode struct {
		name   string
		single int
	}
	fieldList := []fieldNode{}
	// loop, get field names and singles
	for fields.Next() {
		node := fieldNode{}
		err = fields.Scan(&node.name, &node.single)
		if err != nil {
			return nil, err
		}
		fieldList = append(fieldList, node)
	}
	// loop, load each field
	objData = make(map[string]interface{}, 0)
	for _, field := range fieldList {
		value, err := h.LoadFieldItems(tx, objID, field.name)
		if err != nil {
			return nil, err
		}
		if field.single > 0 {
			// Single item, return as scalar
			if len(value) > 0 {
				objData[field.name] = value[0]
			} else {
				objData[field.name] = nil
			}
		} else {
			// Possibly multi-item, return as list
			objData[field.name] = value
		}
	}
	return objData, nil
}

func (h *HathiDB) LoadFieldItems(tx *sql.Tx, objID string, field string) (value []interface{}, err error) {
	if tx == nil {
		tx, err = h.BeginTransaction()
		if err != nil {
			return nil, err
		}
		defer h.EndTransaction(tx, &err)
	}
	// Retrieve field values
	query := "select TYPE, STRING, INTEGER, FLOAT, BOOL, EMBED from FIELDDATA where ID = $1 and NAME = $2 order by ITEMINDEX asc;"
	data, err := tx.Query(query, objID, field)
	if err != nil {
		return nil, err
	}
	defer data.Close()
	var vType int
	var temp interface{}
	// We start with a list, but if single==1 it will be turned into a scalar
	value = make([]interface{}, 0)
	for data.Next() {
		temp, vType, err = loadValue(data)
		if err != nil {
			return nil, err
		}
		if vType == EMBED {
			// Retreive sub-object
			temp, err = h.LoadObjectData(tx, temp.(string))
			if err != nil {
				return nil, err
			}
		}
		value = append(value, temp)
	}
	// Check for missing data
	if len(value) == 0 {
		errText := "No FIELDDATA entries found for " + string(objID) + " " + field
		err = errors.New(errText)
	}
	return value, nil
}
