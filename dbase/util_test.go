package dbase

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	//"fmt"
	"reflect"
	"testing"

	//"gitlab.com/hathi-social/hathi-server/activitypub"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

func Test_getAllTableIDs(t *testing.T) {
	var err error
	var db *HathiDB

	db = new(HathiDB)
	mockedDB, mock, err := sqlmock.New()
	db.DB = mockedDB

	// Test GetAllObjectIDs
	// Set expectations
	objRows := sqlmock.NewRows([]string{"ID"}).
		AddRow("a").
		AddRow("b").
		AddRow("c")
	mock.ExpectBegin()
	mock.ExpectQuery(`select ID from FOO`).
		WillReturnRows(objRows)
	mock.ExpectCommit()
	// Run test
	ids, err := db.getAllTableIDs(nil, "FOO")
	if err != nil {
		t.Fatal("Database getAllTableIDs:", err)
	}
	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database getAllTableIDs: unfulfilled expectations:", err)
	}
	if reflect.DeepEqual([]string{"a", "b", "c"}, ids) == false {
		t.Fatal("Database getAllTableIDs: incorrect values:", ids)
	}
}

func TestValueToTypes(t *testing.T) {
	// Test null
	tC, vS, vI, vF, vB, vE, err := ValueToTypes(nil)
	if tC != NULL || err != nil {
		t.Fatal("ValueToTypes failure:", tC, vS, vI, vF, vB, vE, err)
	}
	// Test string
	tC, vS, vI, vF, vB, vE, err = ValueToTypes("foo")
	if tC != STRING || vS != "foo" || err != nil {
		t.Fatal("ValueToTypes failure:", tC, vS, vI, vF, vB, vE, err)
	}
	// Test integer
	tC, vS, vI, vF, vB, vE, err = ValueToTypes(42)
	if tC != INTEGER || vI != 42 || err != nil {
		t.Fatal("ValueToTypes failure:", tC, vS, vI, vF, vB, vE, err)
	}
	// Test float
	tC, vS, vI, vF, vB, vE, err = ValueToTypes(16.0)
	if tC != FLOAT || vF != 16.0 || err != nil {
		t.Fatal("ValueToTypes failure:", tC, vS, vI, vF, vB, vE, err)
	}
	// Test boolean
	tC, vS, vI, vF, vB, vE, err = ValueToTypes(true)
	if tC != BOOL || vB != true || err != nil {
		t.Fatal("ValueToTypes failure:", tC, vS, vI, vF, vB, vE, err)
	}
	// Test embedded, no id
	tC, vS, vI, vF, vB, vE, err = ValueToTypes(
		map[string]interface{}{"foo": "bar"})
	if tC != EMBED || vE != "" || err != nil {
		t.Fatal("ValueToTypes failure:", tC, vS, vI, vF, vB, vE, err)
	}
	// Test embedded, with @id
	tC, vS, vI, vF, vB, vE, err = ValueToTypes(
		map[string]interface{}{"@id": "bar"})
	if tC != EMBED || vE != "bar" || err != nil {
		t.Fatal("ValueToTypes failure:", tC, vS, vI, vF, vB, vE, err)
	}
	// Test embedded, with alt id
	tC, vS, vI, vF, vB, vE, err = ValueToTypes(
		map[string]interface{}{"id": "bar"})
	if tC != EMBED || vE != "bar" || err != nil {
		t.Fatal("ValueToTypes failure:", tC, vS, vI, vF, vB, vE, err)
	}
	// Test invalid type
	tC, vS, vI, vF, vB, vE, err = ValueToTypes(
		map[bool]bool{true: false, false: true})
	if tC != 0 || err == nil {
		t.Fatal("ValueToTypes failure:", tC, vS, vI, vF, vB, vE, err)
	}
}

func TestTypesToValue(t *testing.T) {
	var res interface{}
	// Test null
	res, err := TypesToValue(NULL, "foo", 42, 16.0, true, "bar")
	if err != nil {
		t.Fatal("TypesToValue string failure", err)
	}
	if res != nil {
		t.Fatal("TypesToValue string failure:", res)
	}
	// Test string
	res, err = TypesToValue(STRING, "foo", 42, 16.0, true, "bar")
	if err != nil {
		t.Fatal("TypesToValue string failure", err)
	}
	if res.(string) != "foo" {
		t.Fatal("TypesToValue string failure:", res)
	}
	// Test integer
	res, err = TypesToValue(INTEGER, "foo", 42, 16.0, true, "bar")
	if err != nil {
		t.Fatal("TypesToValue integer failure", err)
	}
	if res.(int64) != 42 {
		t.Fatal("TypesToValue integer failure:", res)
	}
	// Test float
	res, err = TypesToValue(FLOAT, "foo", 42, 16.0, true, "bar")
	if err != nil {
		t.Fatal("TypesToValue float failure", err)
	}
	if res.(float64) != 16.0 {
		t.Fatal("TypesToValue float failure:", res)
	}
	// Test boolean
	res, err = TypesToValue(BOOL, "foo", 42, 16.0, true, "bar")
	if err != nil {
		t.Fatal("TypesToValue boolean failure", err)
	}
	if res.(bool) != true {
		t.Fatal("TypesToValue boolean failure:", res)
	}
	// Test embed
	res, err = TypesToValue(EMBED, "foo", 42, 16.0, true, "bar")
	if err != nil {
		t.Fatal("TypesToValue embed failure", err)
	}
	if res.(string) != "bar" {
		t.Fatal("TypesToValue embed failure:", res)
	}
	// Test invalid type
	res, err = TypesToValue(-1, "foo", 42, 16.0, true, "bar")
	if err == nil {
		t.Fatal("TypesToValue integer failure", err)
	}
}

func TestGenerateIDCode(t *testing.T) {
	var db *HathiDB

	db = new(HathiDB)
	mockedDB, mock, err := sqlmock.New()
	db.DB = mockedDB

	// Test existing lastID
	mock.ExpectBegin()
	langRows := sqlmock.NewRows([]string{"LASTID"}).
		AddRow(10)
	mock.ExpectQuery("select LASTID from TRANSID").
		WithArgs("foo").
		WillReturnRows(langRows)
	mock.ExpectExec("update TRANSID").
		WithArgs(11, "foo").
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()
	newID, err := db.GenerateIDCode(nil, "foo")
	if newID != "B" || err != nil {
		t.Fatal("GenerateEmbedID failure:", newID, err)
	}

	// Test missing lastID
	mock.ExpectBegin()
	langRows = sqlmock.NewRows([]string{"LASTID"})
	mock.ExpectQuery("select LASTID from TRANSID").
		WithArgs("foo").
		WillReturnRows(langRows)
	mock.ExpectExec("update TRANSID").
		WithArgs(1, "foo").
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()
	newID, err = db.GenerateIDCode(nil, "foo")
	if newID != "1" || err != nil {
		t.Fatal("GenerateEmbedID failure:", newID, err)
	}
}
