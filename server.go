package main

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"strconv"

	"gitlab.com/hathi-social/hathi-server/activitypub"
	"gitlab.com/hathi-social/hathi-server/dbase"
	"gitlab.com/hathi-social/hathi-server/handlers"
	"gitlab.com/hathi-social/hathi-server/hathi"
	"gitlab.com/hathi-social/hathi-protocol"
)

func main() {
	var hostname string
	var port int
	var logging bool
	var dumping, restoring string

	// Setup command line parsing
	flag.StringVar(&hostname, "host", "localhost", "Hostname of the server")
	flag.IntVar(&port, "port", 8080, "Port to listen on")
	flag.BoolVar(&logging, "log", false, "Turns on error logging")
	flag.StringVar(&dumping, "dump", "",
		"Dumps the database to the specified JSON file")
	flag.StringVar(&restoring, "restore", "",
		"Restores the database from the specified JSON file")

	flag.Parse()

	// Setup logging
	hathi.Logging = logging
	activitypub.Logging = logging
	handlers.Logging = logging
	dbase.Logging = logging
	if logging {
		protocol.SetLogLevel(5)
	}

	// Initialize Hathi Core
	hathi.InitEngine(hostname, strconv.Itoa(port), "hathi.db")

	// If running to dump or load a database do that
	if dumping != "" {
		buf, err := DoDump()
		if err != nil {
			log.Fatal(err)
		}
		bees := buf.Bytes()
		err = ioutil.WriteFile(dumping, bees, 0644)
		if err != nil {
			log.Fatal(err)
		}
		return
	} else if restoring != "" {
		data, err := ioutil.ReadFile(restoring)
		if err != nil {
			log.Fatal(err)
		}
		err = DoRestore(data)
		if err != nil {
			log.Fatal(err)
		}
		log.Println("Database loaded from", restoring)
		return
	}

	// If we are here we are running normally.

	// Spawn our remote propagation handler
	// Disabled while focusing on single-server
	// go hathi.Engine.DoRemotePropagation()

	log.Println("Starting server...")
	// Set up listening
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		log.Println("Failed to listen:", err)
		return
	}
	for { // We only handle a single connection to the router
		conn, err := listener.Accept()
		if err != nil {
			log.Println("Failed to accept:", err)
			continue
		}
		log.Println("Got connection:", conn)
		handlers.HandleConnection(hathi.Engine, conn)
		conn.Close()
	}
}

func DoDump() (buf *bytes.Buffer, err error) {
	dump, err := hathi.Engine.DumpDB()
	if err != nil {
		return nil, err
	}
	// Prepare Objects
	objdata := make([]map[string]interface{}, 0)
	for _, obj := range dump.Objects {
		mobj := obj.Mapify()
		objdata = append(objdata, mobj)
	}
	data := make(map[string]interface{})
	data["objects"] = objdata
	data["privileges"] = dump.Privileges
	dumpInfo := make(map[string]interface{})
	dumpInfo["major_version"] = dump.DumpInfo.MajorVersion
	dumpInfo["minor_version"] = dump.DumpInfo.MinorVersion
	dumpInfo["bugfix_version"] = dump.DumpInfo.BugFixVersion
	dumpInfo["dump_time"] = dump.DumpInfo.DumpTime
	data["dump_info"] = dumpInfo
	// Marshal
	unpretty, err := json.Marshal(data)
	// Create pretty form
	buf = bytes.NewBuffer(nil)
	json.Indent(buf, unpretty, "", "  ")
	return buf, nil
}

func DoRestore(data []byte) (err error) {
	var tmp interface{}
	err = json.Unmarshal(data, &tmp)
	if err != nil {
		return err
	}
	jsonData := tmp.(map[string]interface{})
	dump := hathi.DumpData{}
	// Parse Objects
	dump.Objects = make([]*activitypub.Object, 0)
	// Loop through..... crap what about sub-objects?
	for _, tmp := range jsonData["objects"].([]interface{}) {
		obj := tmp.(map[string]interface{})
		decoded, _ := activitypub.Objectify(obj)
		// We are ignoring the sub-objects for now. A dump won't have them.
		// TODO: The Great Refactor will change this
		if err != nil {
			return err
		}
		dump.Objects = append(dump.Objects, decoded)
	}
	// Parse Privileges
	dump.Privileges = make(map[string]map[string][]string, 0)
	// Loop through each object
	for objID, privs := range jsonData["privileges"].(map[string]interface{}) {
		privSet := make(map[string][]string, 0)
		// Loop through each actor associated with that object
		for actorID, tmp := range privs.(map[string]interface{}) {
			interList := tmp.([]interface{})
			bitList := make([]string, 0)
			// Have to loop through the bits and make them strings
			for _, bit := range interList {
				bitList = append(bitList, bit.(string))
			}
			privSet[actorID] = bitList
		}
		dump.Privileges[objID] = privSet
	}
	err = hathi.Engine.LoadDB(&dump)
	return nil
}
